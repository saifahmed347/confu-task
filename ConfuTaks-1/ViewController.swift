//
//  ViewController.swift
//  ConfuTaks-1
//
//  Created by Saif Shaikh on 20/06/2021.
//

import UIKit
import Foundation

class ViewController: UIViewController {
    @IBOutlet weak var firstImgView: UIImageView!
    @IBOutlet weak var scndImgView: UIImageView!
    @IBOutlet weak var thirdImgView: UIImageView!
    
    var imageUrls: [String] = ["https://media.istockphoto.com/photos/pakistan-monument-islamabad-picture-id535695503?k=6&m=535695503&s=612x612&w=0&h=uP8aDK4xlfjk3kEiyr9wwUiuh80UwAiICweFpiBDosk=", "https://st.depositphotos.com/1428083/2946/i/600/depositphotos_29460297-stock-photo-bird-cage.jpg", "https://images.indianexpress.com/2019/04/gettyimages-956272022-759.jpg"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }
    
    @IBAction func startDownloadHandler(_ sender: Any) {
        self.firstImgView.loadImage(urlString: imageUrls[0])
        self.scndImgView.loadImage(urlString: imageUrls[1])
        self.thirdImgView.loadImage(urlString: imageUrls[2])
    }
    
    
    
    
}

extension UIImageView {
    func loadImage(urlString: String) {
        guard let url = URL(string: urlString) else {
            return
        }
        
        DispatchQueue.global().async { [weak self] in
            
            let dataTask = URLSession.shared.dataTask(with: url) { data, url, error in
                if let image = UIImage(data: data!) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
            	
            dataTask.resume()
        }
    }
}

